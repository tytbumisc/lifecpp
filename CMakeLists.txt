cmake_minimum_required(VERSION 3.23)
project(lifecpp)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

# gui lib stuff

set(NANOGUI_BUILD_EXAMPLE OFF CACHE BOOL " " FORCE)
set(NANOGUI_BUILD_PYTHON  OFF CACHE BOOL " " FORCE)
set(NANOGUI_INSTALL       OFF CACHE BOOL " " FORCE)

add_subdirectory(lib/nanogui)

set_property(TARGET glfw glfw_objects nanogui PROPERTY FOLDER "dependencies")

add_definitions(${NANOGUI_EXTRA_DEFS})

include_directories(${NANOGUI_EXTRA_INCS})


file(GLOB SOURCES "src/*.cpp")
add_executable(${PROJECT_NAME} ${SOURCES})



target_link_libraries(${PROJECT_NAME} nanogui ${NANOGUI_EXTRA_LIBS})
set_property(TARGET ${PROJECT_NAME} PROPERTY CXX_STANDARD 23)

