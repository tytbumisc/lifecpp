#pragma once
#include <vector>
#include <memory>

#define VEC2D std::vector<std::vector<bool>>

namespace Life{
	void updateInPlace(VEC2D *arr);

	/*
	 * updates arr in [splits] parallel threads
	 *
	 *
	 */
	void updateInPlaceThreaded(VEC2D *arr, int splits);
	
	/*
	 *returns an updates horizontal slice of the given vector
	 *arr: previous generation vector
	 *start: index of the first row to update
	 *end: index of the first row to not update, or one plus the index of the last row to update
	 */
	std::vector<std::vector<bool>> updatePartial(const VEC2D *arr, int start, int end);

	std::vector<std::vector<bool>> getRandomVector(int vertical, int horizontal, float chance);


	void drawArr(std::shared_ptr<VEC2D> arr);
}
