#include "lifeview.h"
#include "life.h"
#include "nanogui/imageview.h"
#include "nanogui/texture.h"
#include "nanogui/vector.h"
#include "nanogui/widget.h"
#include <chrono>
#include <memory>
#include <stdexcept>

using namespace Life;


std::vector<uint8_t> getImageData(VEC2D *arr, int onColor, int offColor);

LifeView::LifeView(nanogui::Widget *parent, int vertical, int horizontal) : nanogui::ImageView(parent){
	//initialize pointers
	frameBuffer = std::make_unique<std::queue<std::vector<uint8_t>>>(std::queue<std::vector<uint8_t>>());
	currentFrame = std::make_unique<std::vector<uint8_t>>(std::vector<uint8_t>());
	currentRenderingFrame = std::make_unique<std::future<std::vector<uint8_t>>>(std::future<std::vector<uint8_t>>());
	frameCounter = std::make_unique<std::queue<std::chrono::time_point<std::chrono::high_resolution_clock>>> (std::queue<std::chrono::time_point<std::chrono::high_resolution_clock>>());
        currentInsert = std::make_unique<VEC2D>(VEC2D()); 


	resetArr(vertical, horizontal);
	setTexture(vertical, horizontal);
	lastUpdate = std::chrono::high_resolution_clock::now();
}



void LifeView::setRun(bool isRun){
	run = isRun;
}
void LifeView::toggleRun(){
	setRun(!run);
}

void LifeView::setSplits(int nsplits){
  	splits = nsplits;
}

void LifeView::advance(){
	displayNextFrame();
	bufferNextFrame();
}

bool LifeView::mouse_button_event(const nanogui::Vector2i &p, int button, bool down, int modifiers){
	if (button == 0 && down){
		nanogui::Vector2f v2 = nanogui::Vector2f(static_cast<float>(p.x()), static_cast<float>(p.y()));
	        nanogui::Vector2i coords = nanogui::ImageView::pos_to_pixel(v2);
	        try{
		insertPartial(&*currentInsert, coords.y(), coords.x());
		} catch (std::invalid_argument){
			std::puts("Inserted partial goes out of bounds!");
			return false;
		}
	}
	return true;
}

void LifeView::setNewDimensions(int vertical, int horizontal){ 
	std::unique_ptr<VEC2D> newVec = std::make_unique<VEC2D> (VEC2D());
	newVec->reserve(vertical);
	for (int row = 0; row < vertical; row++){
		std::vector<bool> r;
		r.reserve(horizontal);
		if (row < mainArr->size()){
			for (int col = 0; col < horizontal; col++){
				if (col < mainArr->at(row).size()){
					r.push_back((*mainArr)[row][col]);
				} else {
					r.push_back(false);
				}
			}
		} else { //add new blank rows
			for (int col = 0; col < horizontal; col++){
				r.push_back(false);
			}
		}
		newVec->push_back(r);
	}

	if (vertical < splits){
		splits = vertical;
	}
	std::swap(mainArr, newVec);
	setTexture(vertical, horizontal);
	clearFrameBuffer();
}

void LifeView::insertPartial(const VEC2D *insert, int vertical, int horizontal){
	if (vertical + insert->size() > mainArr->size() or horizontal + insert->at(0).size() > mainArr->at(0).size())
		throw std::invalid_argument("Inserted array goes out of bounds");
	for (int row = 0; row < insert->size(); row++){
		for (int col = 0; col < insert->at(0).size(); col++){
			(*mainArr)[row + vertical][col + horizontal] = (*insert)[row][col];
		}
	}
}


void LifeView::setTexture(int vertical, int horizontal){	
    nanogui::Texture *tex = new nanogui::Texture(nanogui::Texture::PixelFormat::RGB, nanogui::Texture::ComponentFormat::UInt8, nanogui::Vector2i(horizontal, vertical),
		    nanogui::Texture::InterpolationMode::Nearest,
		    nanogui::Texture::InterpolationMode::Nearest,
		    nanogui::Texture::WrapMode::Repeat,
		    1,
		    (uint8_t) nanogui::Texture::TextureFlags::ShaderRead,
		    false);
    set_image(tex);

}

void LifeView::setOnColor(int color){
	onColor = color;
}

void LifeView::setOffColor(int color){
	offColor = color;
}
void LifeView::draw_contents(){
	auto now = std::chrono::high_resolution_clock::now();
	//continuously buffer new frames
	auto t = std::chrono::high_resolution_clock::to_time_t(now);
	bufferNextFrame();
	
	if ((std::chrono::duration_cast<std::chrono::milliseconds> (now - lastUpdate)).count() > waitSeconds * 1000 and run){ //run each frame as set by refresh rate
		lastUpdate = now;
		if (frameBuffer->size() >= 1){
			displayNextFrame();
			frameCounter->push(now); //add new frame timestamps
		}
	}
	if (frameCounter->size() >= 1 and frameCounter->front() < now - std::chrono::seconds(1)){ //remove old frame timestamps
		frameCounter->pop();
	}
	nanogui::ImageView::draw_contents();
}

void LifeView::setRefresh(float time){
	waitSeconds = time;
}

void LifeView::setFrameBufferSize(int size){
	frameBufferSize = size;
}

void LifeView::setRandomize(bool toRandomize){
	randomize = toRandomize;
}

void LifeView::setRandomizeChance(float rChance){
	chance = rChance;
}

void LifeView::setInsert(std::unique_ptr<VEC2D> insert){
        currentInsert = std::move(insert);
}

int LifeView::getVertical(){
	return mainArr->size();
}

int LifeView::getHorizontal(){
	return mainArr->at(0).size();
}

int LifeView::getFPS(){
	return frameCounter->size();
}


void LifeView::bufferNextFrame(){
	//don't buffer if framebuffer is full or if the previous frame isnt done rendering
	if (frameBuffer->size() < frameBufferSize and currentRenderingFrame->valid() and currentRenderingFrame->wait_for(std::chrono::seconds(0)) == std::future_status::ready ){
		frameBuffer->push(std::move(currentRenderingFrame->get())); //only push frame if there is a rendered frame ready
		currentRenderingFrame = std::make_unique<std::future<std::vector<uint8_t>>>(std::async(std::launch::async,
			[&] () {
 	 		updateInPlaceThreaded(&*mainArr, splits); 
			return getImageData(&*mainArr, onColor, offColor);
			}));	
	}
	if (!currentRenderingFrame->valid()){ //render frame if there is none already
		currentRenderingFrame = std::make_unique<std::future<std::vector<uint8_t>>>(std::async(std::launch::async,
			[&] () {
 	 		updateInPlaceThreaded(&*mainArr, splits);
			return getImageData(&*mainArr, onColor, offColor);
			}));
	}
}

void LifeView::displayNextFrame(){
	if (frameBuffer->size() > 0){
		frameCount++;
	currentFrame = std::make_unique<std::vector<uint8_t>>(frameBuffer->front()); //move current frame to its own variable so that -> 
	m_image->upload(currentFrame->data()); //the current frame data points to its actual location instead of just wherever -> 
	frameBuffer->pop(); //otherwise depending on how upload works then the frame vector going out of scope here might have caused undefined behavior when .data() points to nothing
	}
}

void LifeView::clearFrameBuffer(){
	if (frameBuffer->size() > 0){
		auto empty = std::make_unique<std::queue<std::vector<uint8_t>>>(std::queue<std::vector<uint8_t>>());
		std::swap(frameBuffer, empty);
	}
	currentRenderingFrame = std::make_unique<std::future<std::vector<uint8_t>>>(std::future<std::vector<uint8_t>>());
}
std::vector<uint8_t> getImageData(const VEC2D *arr, int onColor, int offColor){
	int vertical = arr->size();
	int horizontal = arr->at(0).size();
	int onR = (onColor >> 16) & 0xff;
	int offR = (offColor >> 16) & 0xff;
	int onG = (onColor >> 8) & 0xff;
	int offG = (offColor >> 8) & 0xff;
	int onB = (onColor) & 0xff;
	int offB = (offColor) & 0xff;
	
    int pix = vertical * horizontal;
    std::vector<uint8_t> pixelData;
    pixelData.reserve(pix * 3);
    for (int row = 0; row < vertical; row++){
	    for (int col = 0; col < horizontal; col++){
		if ((*arr)[row][col]){
			pixelData.push_back(onR);
			pixelData.push_back(onG);
			pixelData.push_back(onB);
		}
		else{
			pixelData.push_back(offR);
			pixelData.push_back(offG);
			pixelData.push_back(offB);
		}
	    }
    }
	return pixelData;
}

void LifeView::resetArr(int vertical, int horizontal){
        if (randomize){
		VEC2D randArr = Life::getRandomVector(vertical, horizontal, chance);
    		auto randArrP = std::make_unique<VEC2D>(randArr);
		mainArr.swap(randArrP);
	}else{
		std::unique_ptr<VEC2D> newArr = std::make_unique<VEC2D>(VEC2D());
		newArr->reserve(vertical);
		for (int row = 0; row < vertical; row++){
			std::vector<bool> r;
			r.reserve(horizontal);
			for (int col = 0; col < horizontal; col++){
				r.push_back(false);
			}
		newArr->push_back(r);
		}
		mainArr.swap(newArr);
	}

	setTexture(vertical, horizontal);
	clearFrameBuffer();

}

void LifeView::resetArr(){
	resetArr(mainArr->size(),mainArr->at(0).size());
}
