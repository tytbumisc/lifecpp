#include "varilabel.h"
#include <chrono>



VariLabel::VariLabel(nanogui::Widget *parent, std::function<std::string()> valueGetter, int refreshMs) : nanogui::Label(parent, "Sample Text"){
	value = std::move(valueGetter);
	updateTime = refreshMs;
}

void VariLabel::draw(NVGcontext *ctx){
	auto now = std::chrono::high_resolution_clock::now();
	if (now > (lastUpdate + std::chrono::milliseconds(updateTime))){
		nanogui::Label::set_caption(value());
		lastUpdate = std::move(now);
	}
	nanogui::Label::draw(ctx);
}
