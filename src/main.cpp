#include "life.h"
#include "lifeview.h"
#include "nanogui/button.h"
#include "nanogui/canvas.h"
#include "nanogui/common.h"
#include "nanogui/imageview.h"
#include "nanogui/label.h"
#include "nanogui/textbox.h"
#include "nanogui/texture.h"
#include "nanogui/vector.h"
#include "nanogui/widget.h"
#include "varilabel.h"

#include <memory>
#include <nanogui/screen.h>
#include <nanogui/window.h>
#include <nanogui/layout.h>
#include <stdexcept>
#include <string>

std::vector<uint8_t> getImageData(VEC2D *arr, int onColor, int offColor);


using namespace nanogui;
int main(){
	int vertical = 100;
	int horizontal = 100;
	int splits = 3;
        int onColor = 0xff0088;
	int offColor = 0x000055;

	std::vector<std::string> gun = {
		"000000000000000000000000000000000000000",
		"000000000000000000000000010000000000000",
		"000000000000000000000001010000000000000",
		"000000000000011000000110000000000001100",
		"000000000000100010000110000000000001100",
		"011000000001000001000110000000000000000",
		"011000000001000101100001010000000000000",
		"000000000001000001000000010000000000000",
		"000000000000100010000000000000000000000",
		"000000000000011000000000000000000000000",
		"000000000000000000000000000000000000000",
	};

	VEC2D gun2;



	for (int row = 0; row < gun.size(); row++){
		auto r = gun.at(row);
		std::vector<bool> r2;
		for (int col = 0; col < r.size(); col++){
			char c = r.c_str()[col];
			r2.push_back((c == '0') ? false : true);
		}
		gun2.push_back(r2);
	}
	

    	

	init();
	Screen *app = new Screen(Vector2i(1920,1080), "Life");
	Window *buttonWin = new Window(app, "buttons");
	buttonWin->set_position(Vector2i(15,15));
	buttonWin->set_layout(new GroupLayout());
        

	Window *imgWin = new Window(app, "view");
        imgWin->set_position(Vector2i(200,40));
	imgWin->set_layout(new GroupLayout());

        Window *diagWin = new Window(app, "diagnostics");
	diagWin->set_position(Vector2i(15,1300));
	diagWin->set_layout(new GroupLayout());
        

	Window *settingsWin = new Window(app, "settings");
	settingsWin->set_position(Vector2i(15,300));
	settingsWin->set_layout(new GroupLayout());

	Window *partialRandWin = new Window(app, "partial randomize");
	partialRandWin->set_position(Vector2i(15, 700));
	partialRandWin->set_layout(new GroupLayout());

	Life::LifeView *lifeView = new Life::LifeView(imgWin, vertical, horizontal);
    	lifeView->set_height(1080);
	lifeView->set_width(1920);
	lifeView->setOffColor(offColor);
	lifeView->setOnColor(onColor);
	lifeView->setInsert(std::make_unique<VEC2D>(VEC2D(gun2)));

  

	Button *advButton = new Button(buttonWin, "Advance");
	advButton->set_position(Vector2i(100,20));
	advButton->set_tooltip("Advance to the next generation");
	advButton->set_callback([&] {lifeView->advance();});
	

	Button *pauseButton = new Button(buttonWin, "Run");
	pauseButton->set_flags(0b00100);
	pauseButton->set_tooltip("Start advancing continuously");
	pauseButton->set_change_callback([&](bool push){
		lifeView->setRun(push);
		pauseButton->set_caption((push) ? "Stop" : "Start");
		pauseButton->set_tooltip((push) ? "Stop advancing continuously" : "Start advancing continuously");
		});

	Button *randomizeB1 = new Button(buttonWin, "Randomize");
	randomizeB1->set_flags(nanogui::Button::RadioButton);
	std::function<void()> rb1 = [&] {
			lifeView->setRandomize(true);
			};
	randomizeB1->set_callback(rb1);

	Button *randomizeB2 = new Button(buttonWin, "Blank");
	randomizeB2->set_flags(nanogui::Button::RadioButton);
	std::function<void()> rb2 = [&] {
			lifeView->setRandomize(false);
			};
	randomizeB2->set_callback(rb2);
	
	Button *resetButton = new Button(buttonWin, "Reset");
	resetButton->set_tooltip("Reset the board");
	resetButton->set_callback([&] {lifeView->resetArr();});

	Button *addGliderGunButton = new Button(buttonWin, "Add Glider Gun");
	addGliderGunButton->set_tooltip("add a glider gun to the field");
	addGliderGunButton->set_callback([&] {lifeView->insertPartial(&gun2);});

	std::function<std::string()> fpsFunc =  [&] {return "FPS: " + std::to_string(lifeView->getFPS());};
	VariLabel *FPSLabel = new VariLabel(diagWin, fpsFunc, 50);

        Label *vLabel = new Label(settingsWin, "Vertical Size");
	TextBox *vertBox = new TextBox(settingsWin, "100");
	vertBox->set_editable(true);
	vertBox->set_placeholder("Vertical");
	vertBox->set_default_value("100");
	std::function<bool(const std::string&)> setV = [&](const std::string str){
		try{
			int vert = std::stoi(str);
			if (vert > 0){
				lifeView->setNewDimensions(vert, lifeView->getHorizontal());
				return true;
			}
			return false;

		}
		catch (std::invalid_argument){
			return false;
		}
        };
	vertBox->set_callback(setV);

        Label *hLabel = new Label(settingsWin, "Horizontal Size");
	TextBox *horizBox = new TextBox(settingsWin, "100");
	horizBox->set_editable(true);
	horizBox->set_placeholder("Horizontal");
	horizBox->set_default_value("100");
	std::function<bool(const std::string&)> setH = [&](const std::string str){
		try{
			int horiz = std::stoi(str);
			if (horiz > 0){
				lifeView->setNewDimensions(lifeView->getVertical(), horiz);
				return true;
			}
			return false;

		}
		catch (std::invalid_argument){
			return false;
		}
	};
	horizBox->set_callback(setH);



   	Label *splitLabel = new Label(settingsWin, "Thread Splits");

        TextBox *splitBox = new TextBox(settingsWin, "10");
	splitBox->set_editable(true);
	splitBox->set_placeholder("Splits");
	splitBox->set_default_value("10");
	
	//splitBox->set_format("\d+");
	std::function<bool(const std::string& str)> sspl = [&] (const std::string& str) {
		int val = std::stoi(str);
                                      if (val > 0 and val < lifeView->getVertical()){
                                                  lifeView->setSplits(val);
						  return true;
           					}
				      return false;
        };

	splitBox->set_callback(sspl);
  

        Label *fpsLabel = new Label(settingsWin, "FPS");

        TextBox *fpsBox = new TextBox(settingsWin, "20");
	fpsBox->set_placeholder("FPS");
	fpsBox->set_editable(true) ;
	fpsBox->set_default_value("20");

	std::function<bool(const std::string& str)> sFps = [&] (const std::string& str){
             	try{
			float fps = std::stof(str);
			lifeView->setRefresh(1.0/fps);
			return true;
		}
		catch (const std::invalid_argument){
    	 		return false;
		}
	};
	fpsBox->set_callback(sFps);
	
        Label *chanceLabel = new Label(settingsWin, "Randomization chance");
	TextBox *chanceBox = new TextBox(settingsWin, "0.2");
	chanceBox->set_default_value("0.2");
	chanceBox->set_editable(true);
	std::function<bool(const std::string&)> sCh = [&] (const std::string& str){
		try {
			float ch = std::stof(str);
			if (ch >= 0 and  ch <= 1){
				lifeView->setRandomizeChance(ch);
				return true;
			}
			return false;
		} catch (std::invalid_argument){
			return false;
		}
	};
	chanceBox->set_callback(sCh);

        
        Label *startRandLabel = new Label(partialRandWin, "Start Partial Randomize (y)");
	TextBox *startRandBox = new TextBox(partialRandWin, "0");
        Label *startRandLabelX = new Label(partialRandWin, "Start Partial Randomize (x)");
	TextBox *startRandBoxX = new TextBox(partialRandWin, "0");
        Label *endRandLabel = new Label(partialRandWin, "End Partial Randomize (y)");
	TextBox *endRandBox = new TextBox(partialRandWin, "0");
        Label *endRandLabelX = new Label(partialRandWin, "End Partial Randomize (x)");
	TextBox *endRandBoxX = new TextBox(partialRandWin, "0");
	startRandBox->set_default_value("0");
	startRandBox->set_editable(true);
	std::function<bool(const std::string&)> sSR = [&] (const std::string& str){
		try {
			int st = std::stoi(str);
			if (st > std::stoi(endRandBox->value())){
					endRandBox->set_value(std::to_string(st));
					}
			if (st >= 0){
				return true;
			}
			return false;
		} catch (std::invalid_argument){
			std::puts("a");
			return false;
		}
	};
	startRandBox->set_callback(sSR);



	startRandBoxX->set_default_value("0");
	startRandBoxX->set_editable(true);
	std::function<bool(const std::string&)> sSRX = [&] (const std::string& str){
		try {
			int st = std::stoi(str);
			if (st > std::stoi(endRandBoxX->value())){
					endRandBoxX->set_value(std::to_string(st));
					}
			if (st >= 0){
				return true;
			}
			return false;
		} catch (std::invalid_argument){
			return false;
		}
	};
	startRandBoxX->set_callback(sSRX);


	endRandBox->set_default_value("0");
	endRandBox->set_editable(true);
	std::function<bool(const std::string&)> sER = [&] (const std::string& str){
		try {
			int er = std::stoi(str);
			if (er >= std::stoi(startRandBox->value()) and  er <= lifeView->getVertical()){
				return true;
			}
			return false;
		} catch (std::invalid_argument){
			return false;
		}
	};
	endRandBox->set_callback(sER);

	
	endRandBoxX->set_default_value("0");
	endRandBoxX->set_editable(true);
	std::function<bool(const std::string&)> sERX = [&] (const std::string& str){
		try {
			int er = std::stoi(str);
			if (er >= std::stoi(startRandBoxX->value()) and  er <= lifeView->getHorizontal()){
				return true;
			}
			return false;
		} catch (std::invalid_argument){
			return false;
		}
	};
	endRandBoxX->set_callback(sERX);

        Label *chanceLabel2 = new Label(partialRandWin, "Randomization chance");
	TextBox *chanceBox2 = new TextBox(partialRandWin, "0.2");
	chanceBox2->set_default_value("0.2");
	chanceBox2->set_editable(true);
	std::function<bool(const std::string&)> sCh2 = [&] (const std::string& str){
		try {
			float ch = std::stof(str);
			if (ch >= 0 and  ch <= 1){
				return true;
			}
			return false;
		} catch (std::invalid_argument){
			return false;
		}
	};
	chanceBox2->set_callback(sCh2);


	Button *partRandButton= new Button(partialRandWin, "Partial Randomize");
	partRandButton->set_tooltip("Randomize part of the field");
	partRandButton->set_callback([&] () {
			int start = std::stoi(startRandBox->value());
			int end = std::stoi(endRandBox->value());
			int startX = std::stoi(startRandBoxX->value());
			int endX = std::stoi(endRandBoxX->value());
			float chance = std::stof(chanceBox2->value());
			VEC2D randomed = Life::getRandomVector(end - start, endX - startX, chance);
			lifeView->insertPartial(&randomed, start, startX);
			});


	app->set_visible(true);
	app->perform_layout();
	mainloop(1);
	shutdown();
	delete app;
}

std::vector<uint8_t> getImageData(VEC2D *arr, int onColor, int offColor){
	int vertical = arr->size();
	int horizontal = arr->at(0).size();
	int onR = (onColor >> 16) & 0xff;
	int offR = (offColor >> 16) & 0xff;
	int onG = (onColor >> 8) & 0xff;
	int offG = (offColor >> 8) & 0xff;
	int onB = (onColor) & 0xff;
	int offB = (offColor) & 0xff;
	
    int pix = vertical * horizontal;
    std::vector<uint8_t> pixelData;
    pixelData.reserve(pix * 3);
    for (int row = 0; row < vertical; row++){
	    for (int col = 0; col < horizontal; col++){
		if ((*arr)[row][col]){
			pixelData.push_back(onR);
			pixelData.push_back(onG);
			pixelData.push_back(onB);
		}
		else{
			pixelData.push_back(offR);
			pixelData.push_back(offG);
			pixelData.push_back(offB);
		}
	    }
    }
	return pixelData;
}

