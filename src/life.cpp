#include "life.h"
#include <cstdlib>
#include <ctime>
#include <stdexcept>
#include <string>
#include <iostream>
#include <thread>
#include <future>
#include <stdio.h>

using namespace Life;

static int wrap(int inner, int outer){
	if (inner >= outer) //inner is past the final index of outer, ret 0
		return inner - outer;
	else if (inner < 0)
		return inner + outer;
	else
		return inner;
}

bool getNextStatus(const VEC2D *prevGen, int row, int col){
	int vertical = prevGen->size();
	int horizontal = prevGen->at(0).size();

	int neighbors = 0;
	neighbors += static_cast<int>((*prevGen)[wrap(row-1,vertical)][wrap(col-1,horizontal)]);
	neighbors += static_cast<int>((*prevGen)[wrap(row-1,vertical)][wrap(col,horizontal)]);
	neighbors += static_cast<int>((*prevGen)[wrap(row-1,vertical)][wrap(col+1,horizontal)]);
	neighbors += static_cast<int>((*prevGen)[wrap(row,vertical)][wrap(col-1,horizontal)]);
	neighbors += static_cast<int>((*prevGen)[wrap(row,vertical)][wrap(col+1,horizontal)]);
	neighbors += static_cast<int>((*prevGen)[wrap(row+1,vertical)][wrap(col-1,horizontal)]);
	neighbors += static_cast<int>((*prevGen)[wrap(row+1,vertical)][wrap(col,horizontal)]);
	neighbors += static_cast<int>((*prevGen)[wrap(row+1,vertical)][wrap(col+1,horizontal)]);
	return (neighbors == 3 or (neighbors == 2 and (*prevGen)[row][col])); 
}

void Life::updateInPlace(VEC2D *arr){
	int vertical = arr->size();
	int horizontal = arr->at(0).size();
	auto veccopy = *arr;
	for (int row = 0; row < vertical; row++){
		for (int col = 0; col < horizontal; col++){
			(*arr)[row][col] = getNextStatus(&veccopy, row, col);
		}
	}
}


VEC2D Life::updatePartial(const VEC2D *arr, int start, int end){
	int vsize = end - start;
	int vertical = (*arr).size();
	int horizontal = (*arr)[0].size();
	VEC2D ret;
	ret.reserve(vsize);
	for (int row = 0; row < vsize; row++){
		std::vector<bool> r;
		r.reserve(horizontal);
		for (int col = 0; col < horizontal; col++){
			r.push_back(getNextStatus(arr, row + start, col));
		}
		ret.push_back(r);
	}
	return ret;
}

void Life::updateInPlaceThreaded(VEC2D *arr, int splits){
	int vertical = (*arr).size();
	if (splits < 1 or splits > (*arr).size())
		throw std::invalid_argument("splits must be between 1 and the vertical size");
	std::vector<std::future<VEC2D>> futures;
	futures.reserve(splits);
	int splitSize = vertical/splits;
	for (int i = 0; i < splits; i++){
		int start = splitSize * i;
		int end = splitSize * (i+1);
		if (i == splits - 1) //final split end is the last row of arr
			end = vertical;
		futures.push_back(std::async(&updatePartial, arr, start, end));
	}
	for (int j = 0; j < splits; j++){
		VEC2D part = std::move(futures[j].get());
		int startVal = j * splitSize;
		int r = j * splitSize;
		for (auto row : part){
			int c = 0;
			for (bool b : row){
				(*arr)[r][c] = b;
				c++;
			}
			r++;
		}
	}
}

std::vector<std::vector<bool>> Life::getRandomVector(int vertical, int horizontal, float chance){
	srand(std::chrono::system_clock::now().time_since_epoch().count());
	std::vector<std::vector<bool>> vec;
	vec.reserve(vertical);
	for (int row = 0; row < vertical; row++){
		std::vector<bool> r;
		r.reserve(horizontal);
		for (int col = 0; col < horizontal; col++){
			float rng = static_cast<float> (rand()) / static_cast<float>(RAND_MAX);
			r.push_back(static_cast<int>(rng < chance));
		}
		vec.push_back(r);
	}
	return vec;
}

void Life::drawArr(std::shared_ptr<std::vector<std::vector<bool>>> arr){
	for (auto row : *arr){
		std::string r = "";
		for (bool b : row){
			r += (b) ? "O" : " ";
		}
		printf("%s\n", r.c_str());
	}
	printf("\n\n--------------------------------------------------\n");
}
