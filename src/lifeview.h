#pragma once

#include "life.h"
#include "nanogui/common.h"
#include "nanogui/imageview.h"
#include "nanogui/label.h"
#include "nanogui/texture.h"
#include "nanogui/widget.h"
#include <chrono>
#include <vector>
#include <future>
#include <thread>
#include <queue>
#include <iostream>
#include <optional>


namespace Life{
	class LifeView : public nanogui::ImageView{
		std::unique_ptr<VEC2D> mainArr;
		bool run = false;
		bool randomize = true;
		float chance = 0.2;
		int splits = 20;
		int onColor = 0xff00cc;
		int offColor = 0x000055;
		float waitSeconds = 1.0/20.0;
		nanogui::Texture *tex;
		std::chrono::time_point<std::chrono::high_resolution_clock> lastUpdate;
		
		/*
		 *The framebuffer, holds asynchronously-calculated frames in advance.
		 */
		std::unique_ptr<std::queue<std::vector<uint8_t>>> frameBuffer;
		int frameBufferSize = 5;
		std::unique_ptr<std::vector<uint8_t>> currentFrame;
		std::unique_ptr<std::future<std::vector<uint8_t>>> currentRenderingFrame;
		std::unique_ptr<std::queue<std::chrono::time_point<std::chrono::high_resolution_clock>>> frameCounter;
		std::unique_ptr<VEC2D> currentInsert;

	public:
		LifeView(nanogui::Widget *parent, int vertical, int horizontal);
		/*
		 *Advances to the next generation.
		 *Keep in mind that what is shown on screen will be behind what is in the actual array,
		 *by the size of the frame buffer.
		 */
		void advance();
		void draw_contents() override; 
		void resetArr(int vertical, int horizontal);
		void resetArr();
		void setNewDimensions(int vertical, int horizontal);
		void insertPartial(const VEC2D *insert, int verticalOffset=0, int horizontalOffset=0);

		void setRun(bool run);
		void toggleRun();

		bool mouse_button_event(const nanogui::Vector2i &p, int button, bool down, int modifiers) override;
		/*
		 *Sets how many times the board is divided for multithreaded computation
		 *Must be between 1 and the number of rows of the board
		 */
		void setSplits(int splits);
		
		/* sets the time to advance to the next generation when running continuously, in seconds
		* Values lower than 0.001 do not have any effect, as the app itself runs at a maximum
		* refresh rate of 1000 redraws per second, and the continuous advance is driven by the calls
		* to the LifeView's draw_contents() method.
		* Also, there is no way for the generation refresh to happen that quickly
		*/
		void setRefresh(float time);


		/*
		 * Sets the frame buffer size
		 * Setting to high values will use a large amount of memory.
		 */
		void setFrameBufferSize(int size);
		void setOnColor(int onColor);
		void setOffColor(int offColor);
		void setRandomize(bool randomize);
		void setRandomizeChance(float chance);
		void setInsert(std::unique_ptr<VEC2D> insert);
		int getFPS(); 
		int getVertical();
		int getHorizontal();
	private:
		void setTexture(int vertical, int horizontal);

		void bufferNextFrame();
		void displayNextFrame();
		void clearFrameBuffer();
		

		std::chrono::time_point<std::chrono::high_resolution_clock> lastFps;
		int frameCount;
	};
}
