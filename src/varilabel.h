#pragma once
#include "nanogui/label.h"
#include "nanogui/common.h"
#include <chrono>
#include <functional>
#include <string>




class VariLabel : public nanogui::Label{
	std::function<std::string()> value;
	std::chrono::time_point<std::chrono::high_resolution_clock> lastUpdate;
	int updateTime;

	public:
	VariLabel(nanogui::Widget *parent, std::function<std::string()> valueGetter, int refreshMs);
	void draw(NVGcontext *ctx) override;
};
