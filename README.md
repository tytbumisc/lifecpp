Conway's Game of Life simulator written in C++



## Dependencies

[Nanogui] (https://github.com/mitsuba-renderer/nanogui)


## Building

Requires cmake and gcc

# On Unix-type systems
`git clone --recursive https://gitlab.com/tytbumisc/lifecpp`
`cd lifecpp`
`mkdir build && cd build`
Nanogui will not compile correctly with clang, you must use gcc to compile
`CC=/lib/gcc CXX=lib/g++ cmake ../`
`make && ./lifecpp`

